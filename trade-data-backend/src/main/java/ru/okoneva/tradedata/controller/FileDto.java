package ru.okoneva.tradedata.controller;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
class FileDto {
    @NotNull(message = "fileName is required")
    private String fileName;
}
