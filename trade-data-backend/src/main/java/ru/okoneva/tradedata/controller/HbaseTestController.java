package ru.okoneva.tradedata.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import ru.okoneva.tradedata.model.DataItem;
import ru.okoneva.tradedata.service.HbaseService;

import java.util.List;

/**
 * Контроллер для тестирования и отладки
 */
@Slf4j
@RestController
@RequestMapping("${spring.application.name}/api")
@RequiredArgsConstructor
public class HbaseTestController {

    private final HbaseService hbaseService;

    @PutMapping("/table")
    public ResponseEntity<Object> createTable() {
        hbaseService.createTable();
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/table")
    public ResponseEntity<Object> deleteTable() {
        hbaseService.deleteTable();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/item")
    public ResponseEntity<List<DataItem>> getAll() {
        final List<DataItem> result =  hbaseService.getDataItems();
        return ResponseEntity.ok(result);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleException(Exception e, WebRequest request) {
        log.error("Internal server error", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }
}
