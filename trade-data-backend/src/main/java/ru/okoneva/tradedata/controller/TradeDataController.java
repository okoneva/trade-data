package ru.okoneva.tradedata.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import ru.okoneva.tradedata.service.HbaseService;
import ru.okoneva.tradedata.service.HdfsService;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("${spring.application.name}/api")
@RequiredArgsConstructor
public class TradeDataController {

    private final HdfsService hdfsService;
    private final HbaseService hbaseService;

    @PutMapping("/file")
    public ResponseEntity<Object> loadFile(@Valid @RequestBody final FileDto file) throws IOException {
        final String hdfsFilePath = hdfsService.copyToRemote(file.getFileName());
        hdfsService.runJarAsync(hdfsFilePath);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/item")
    public ResponseEntity<Object> loadDataItem(@Valid @RequestBody final DataItemDto dataItemDto) {
        hbaseService.insertDataItem(dataItemDto.toDataItem());
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(FileNotFoundException.class)
    public final ResponseEntity<Object> handleFileNotFoundException(Exception e, WebRequest request) {
        log.error("File not found", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleException(Exception e, WebRequest request) {
        log.error("Internal server error", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }
}
