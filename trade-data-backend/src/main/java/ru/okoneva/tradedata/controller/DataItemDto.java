package ru.okoneva.tradedata.controller;

import lombok.Data;
import ru.okoneva.tradedata.model.DataItem;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.Instant;

@Data
public class DataItemDto {
    @NotNull(message = "traderId is required")
    private long traderId;
    @NotNull(message = "tickerCode is required")
    private String tickerCode;
    @NotNull(message = "sharePrice is required")
    private float sharePrice;
    @NotNull(message = "overallPosition is required")
    private int overallPosition;
    @NotNull(message = "Date of transaction is required")
    private Instant date;

    public DataItem toDataItem() {
        return DataItem.builder()
            .traderId(traderId)
            .tickerCode(tickerCode)
            .sharePrice(sharePrice)
            .overallPosition(overallPosition)
            .date(Date.from(date))
            .build();
    }
}
