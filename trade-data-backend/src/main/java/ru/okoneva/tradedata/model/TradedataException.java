package ru.okoneva.tradedata.model;

public class TradedataException extends RuntimeException {
    public TradedataException(final String message) {
        super(message);
    }

    public TradedataException(final String s, final Exception e) {
        super(s, e);
    }
}
