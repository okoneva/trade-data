package ru.okoneva.tradedata.model;

import lombok.Builder;
import lombok.Value;

import java.util.Date;

@Value
@Builder
public class DataItem {
    private long traderId;
    private String tickerCode;
    private float sharePrice;
    private int overallPosition;
    private Date date;

}
