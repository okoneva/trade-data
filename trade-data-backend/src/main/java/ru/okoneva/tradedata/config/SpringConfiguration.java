package ru.okoneva.tradedata.config;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.hadoop.config.annotation.EnableHadoop;
import org.springframework.data.hadoop.config.annotation.SpringHadoopConfigurerAdapter;
import org.springframework.data.hadoop.config.annotation.builders.HadoopConfigConfigurer;

import javax.annotation.PostConstruct;
import java.net.URI;

@EnableHadoop
@org.springframework.context.annotation.Configuration
public class SpringConfiguration extends SpringHadoopConfigurerAdapter {

    private final String hadoopUser;
    private final String hadoopHomeDir;

    public SpringConfiguration(
        @Value(value = "${tradedata.hadoop.user}") final String hadoopUser,
        @Value(value = "${tradedata.hadoop.home.dir}") final String hadoopHomeDir
    ) {
        this.hadoopUser = hadoopUser;
        this.hadoopHomeDir = hadoopHomeDir;
    }

    @Override
    public void configure(final HadoopConfigConfigurer config) throws Exception {
        config
            .withResources()
            .resource("classpath:/hbase-site.xml")
            .resource("classpath:/core-site.xml");
    }

    @PostConstruct
    public void setProperty() {
        System.setProperty("HADOOP_USER_NAME", hadoopUser);
        System.setProperty("hadoop.home.dir", hadoopHomeDir);
    }

    @Bean
    public FileSystem fileSystem(final Configuration hadoopCfg) throws Exception {
        return FileSystem.get(new URI(hadoopCfg.get("fs.defaultFS")), hadoopCfg, hadoopUser);
    }
}