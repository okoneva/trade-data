package ru.okoneva.tradedata.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Pair;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@Service
@Slf4j
public class RowKeyService {

    public Pair<Long, Date> decodeRowKey(final byte[] rowKey) {
        int middle = rowKey.length / 2;
        long traderId = Long.MAX_VALUE - Bytes.toLong(Arrays.copyOfRange(rowKey, 0, middle));
        long date = Long.MAX_VALUE - Bytes.toLong(Arrays.copyOfRange(rowKey, middle, rowKey.length));
        return Pair.newPair(traderId, new Date(date));
    }

    public byte[] calculateKey(final long traderId, final Date date) {
        final long sec = removeTime(date).getTime();
        log.debug("Building rowKey for {}, {}. corrected date sec = {}",
            traderId, date, sec);
        final byte[] b1 = Bytes.toBytes(Long.MAX_VALUE - traderId);
        final byte[] b2 = Bytes.toBytes(Long.MAX_VALUE - sec);
        final byte[] result = new byte[b1.length + b2.length];
        System.arraycopy(b1, 0, result, 0, b1.length);
        System.arraycopy(b2, 0, result, b1.length, b2.length);
        return result;
    }

    private Date removeTime(Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
