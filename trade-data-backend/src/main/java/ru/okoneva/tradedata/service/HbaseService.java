package ru.okoneva.tradedata.service;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Service;
import ru.okoneva.tradedata.model.DataItem;
import ru.okoneva.tradedata.model.TradedataException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
@Slf4j
public class HbaseService {

    private final Configuration configuration;
    private final RowKeyService rowKeyService;

    // Refer to table metadata names by byte array in the HBase API
    private static final byte[] TABLE_NAME = Bytes.toBytes("tradedata");
    private static final byte[] COLUMN_FAMILY_NAME = Bytes.toBytes("d");
    private static final String COLUMN_NAME_TICKER_CODE = "tc";
    private static final String COLUMN_NAME_SHARE_PRICE = "sp";
    private static final String COLUMN_NAME_OVERALL_POSITION = "op";

    /**
     * Метод создан для тестирования и отладки
     */
    public void createTable() {
        log.debug("Start: create table");
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            final Admin admin = connection.getAdmin();
            final HTableDescriptor descriptor = new HTableDescriptor(TableName.valueOf(TABLE_NAME));
            descriptor.addFamily(new HColumnDescriptor(COLUMN_FAMILY_NAME).setMaxVersions(Integer.MAX_VALUE));
            admin.createTable(descriptor);
            log.debug("End: create table {}", TABLE_NAME);
        } catch (IOException e) {
            throw new TradedataException("Can't create table", e);
        }
    }

    /**
     * Метод создан для тестирования и отладки
     */
    public void deleteTable() {
        log.debug("Start: delete table");
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            final Admin admin = connection.getAdmin();
            admin.disableTable(TableName.valueOf(TABLE_NAME));
            admin.deleteTable(TableName.valueOf(TABLE_NAME));
            log.debug("End: delete table");
        } catch (IOException e) {
            throw new TradedataException("Can't delete table", e);
        }
    }

    public void insertDataItem(final DataItem dataItem) {
        log.debug("Start: insertDataItem {}", dataItem);
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            final Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
            byte[] rowKey = rowKeyService.calculateKey(dataItem.getTraderId(), dataItem.getDate());
            final long ts = dataItem.getDate().getTime();
            final Put put = new Put(rowKey);
            put.addColumn(COLUMN_FAMILY_NAME, Bytes.toBytes(COLUMN_NAME_TICKER_CODE), ts, Bytes.toBytes(dataItem.getTickerCode()));
            put.addColumn(COLUMN_FAMILY_NAME, Bytes.toBytes(COLUMN_NAME_SHARE_PRICE), ts, Bytes.toBytes(dataItem.getSharePrice()));
            put.addColumn(COLUMN_FAMILY_NAME, Bytes.toBytes(COLUMN_NAME_OVERALL_POSITION), ts, Bytes.toBytes(dataItem.getOverallPosition()));
            table.put(put);
            table.close();
            log.debug("End: insertDataItem");
        } catch (IOException e) {
            throw new TradedataException("Can't insert record", e);
        }
    }

    /**
     * Метод создан для тестирования и отладки. Не предполагается использование на больших объемах данных
     */
    public List<DataItem> getDataItems() {
        log.debug("start scan ");
        final List<DataItem> dataItems = new ArrayList<>();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
            Scan scan = new Scan();
            scan.setMaxVersions();
            scan.setMaxResultSize(100);
            ResultScanner scanner = table.getScanner(scan);
            for (Result result = scanner.next(); (result != null); result = scanner.next()) {
                final Long traderId = rowKeyService.decodeRowKey(result.getRow()).getFirst();
                final Map<Long, DataItem.DataItemBuilder> builders = new HashMap<>();
                for (Cell cell : result.listCells()) {
                    final String qualifier = Bytes.toString(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength());
                    final Long ts = cell.getTimestamp();
                    final DataItem.DataItemBuilder builder = builders
                        .computeIfAbsent(ts, key -> DataItem.builder().traderId(traderId).date(new Date(ts)));
                    switch (qualifier) {
                        case COLUMN_NAME_TICKER_CODE:
                            val tc = Bytes.toString(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength());
                            builder.tickerCode(tc);
                            break;
                        case COLUMN_NAME_SHARE_PRICE:
                            val sp = Bytes.toFloat(cell.getValueArray(), cell.getValueOffset());
                            builder.sharePrice(sp);
                            break;
                        case COLUMN_NAME_OVERALL_POSITION:
                            val op = Bytes.toInt(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength());
                            builder.overallPosition(op);
                            break;
                        default:
                            log.error("Unexpected qualifier {}", qualifier);

                    }
                }
                builders.values().stream().map(DataItem.DataItemBuilder::build).forEach(dataItems::add);
            }
            log.debug("End scan");
            return dataItems;
        } catch (IOException e) {
            throw new TradedataException("Can't get DataItem", e);
        }
    }
}
