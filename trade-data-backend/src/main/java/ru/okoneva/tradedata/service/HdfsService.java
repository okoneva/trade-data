package ru.okoneva.tradedata.service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.okoneva.tradedata.model.TradedataException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;

import static java.lang.String.format;

@Service
@Slf4j
public class HdfsService {
    static final String TABLE_NAME = "tradedata";

    private final FileSystem fileSystem;
    private final String hdfsHost;
    private final String hdfsUser;
    private final String hdfsPassword;
    private final String hdfsOutputFolder;
    private final String resourceFolder;
    private final String jarName;
    private final Configuration configuration;

    public HdfsService(
        final FileSystem fileSystem,
        @Value(value = "${tradedata.hdfs.host}") final String hdfsHost,
        @Value(value = "${tradedata.hadoop.user}") final String hdfsUser,
        @Value(value = "${tradedata.hadoop.password}") final String hdfsPassword,
        @Value(value = "${tradedata.hdfs.output.dir}") final String hdfsOutputFolder,
        @Value(value = "${tradedata.hadoop.resource.dir}") final String resourceFolder,
        @Value(value = "${tradedata.bulk.loading.jar}") final String jarName,
        final Configuration configuration
    ) {
        this.fileSystem = fileSystem;
        this.hdfsHost = hdfsHost;
        this.hdfsUser = hdfsUser;
        this.hdfsPassword = hdfsPassword;
        this.hdfsOutputFolder = hdfsOutputFolder;
        this.resourceFolder = resourceFolder;
        this.jarName = jarName;
        this.configuration = configuration;
    }

    public String copyToRemote(final String filePath) throws IOException {
        log.info("Start: copy file {} to hdfs", filePath);
        final InputStream is = Files.newInputStream(Paths.get(filePath));
        final String fileName = FilenameUtils.getName(filePath);
        final Path outputPath = new Path(hdfsOutputFolder, fileName);
        final OutputStream os = fileSystem.create(outputPath);
        IOUtils.copyBytes(is, os, 4096, true);
        log.info("End: copy file {} to hdfs", filePath);
        return outputPath.toString();
    }

    public void runJar(final String inputFilePath) {
            final Path outputPath = new Path(hdfsOutputFolder, String.valueOf(Instant.now().getEpochSecond()));
            runBySsh(inputFilePath, outputPath.toString());
            deleteQuietly(new Path(inputFilePath));
            deleteQuietly(outputPath);
    }

    public void runJarAsync(final String inputFilePath) {
        new Thread(() -> runJar(inputFilePath)).start();
    }

    private void runBySsh(final String inputFilePath, final String outputPath) {
        try {
            final Session session;
            final JSch jsch = new JSch();
            session = jsch.getSession(hdfsUser, hdfsHost);
            session.setPassword(hdfsPassword);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            final Channel channel = session.openChannel("exec");
            final String jarPath = new Path(resourceFolder, jarName).toString();
            final String command = format("hadoop jar %s %s %s", jarPath, inputFilePath, outputPath);
            ((ChannelExec) channel).setCommand(command);

            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);
            InputStream in = channel.getInputStream();
            channel.connect();

            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    System.out.print(new String(tmp, 0, i));
                }
                if (channel.isClosed()) {
                    if (in.available() > 0) continue;
                    System.out.println("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();
        } catch (Exception e) {
            throw new TradedataException("Can't execute ssh command", e);
        }
    }

    private void deleteQuietly(final Path path) {
        try {
            boolean result = fileSystem.delete(path, true);
            log.info("File {} is deleted {} ", path.toString(), result ? "successfully" : "unsuccessfully");
        } catch (IOException e) {
            log.warn("Can't delete {}", path, e);
        }
    }
}
