package ru.okoneva.tradedata.bulkload;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HBaseBulkLoadMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

    static final String TABLE_NAME = "tradedata";

    private static final Logger logger = LoggerFactory.getLogger(HBaseBulkLoadMapper.class);
    private static final String DATA_SEPERATOR = ";";
    private static final byte[] TABLE_NAME_BYTES = Bytes.toBytes(TABLE_NAME);
    private static final byte[] COLUMN_FAMILY_NAME = Bytes.toBytes("d");
    private static final byte[] COLUMN_NAME_TICKER_CODE = Bytes.toBytes("tc");
    private static final byte[] COLUMN_NAME_SHARE_PRICE = Bytes.toBytes("sp");
    private static final byte[] COLUMN_NAME_OVERALL_POSITION = Bytes.toBytes("op");
    private final static int NUM_FIELDS = 5;

    private ImmutableBytesWritable hbaseTableName;

    public void setup(Context context) {
        hbaseTableName = new ImmutableBytesWritable(TABLE_NAME_BYTES);
    }

    public void map(final LongWritable key, final Text value, final Context context) {
        try {
            final String[] values = value.toString().split(DATA_SEPERATOR);
            if (values.length != NUM_FIELDS) {
                return;
            }
            for (String val : values) {
                if (val == null || val.isEmpty()) {
                    return;
                }
            }
            final String tickerCode = values[0];
            final Long traderId = Long.valueOf(values[1]);
            final Float sharePrice = Float.valueOf(values[3]);
            final Integer overallPosition = Integer.valueOf(values[4]);
            final Date date = parseDate(values[2]);
            final byte[] rowKey = calculateKey(traderId, date);
            final long ts = date.getTime();
            final Put put = new Put(rowKey);
            put.addColumn(COLUMN_FAMILY_NAME, COLUMN_NAME_TICKER_CODE, ts, Bytes.toBytes(tickerCode));
            put.addColumn(COLUMN_FAMILY_NAME, COLUMN_NAME_SHARE_PRICE, ts, Bytes.toBytes(sharePrice));
            put.addColumn(COLUMN_FAMILY_NAME, COLUMN_NAME_OVERALL_POSITION, ts, Bytes.toBytes(overallPosition));
            context.write(hbaseTableName, put);
        } catch (Exception e) {
            logger.error("Can't map row", e);
        }
    }

    private byte[] calculateKey(final long traderId, final Date date) {
        final long sec = removeTime(date).getTime();
        final byte[] b1 = Bytes.toBytes(Long.MAX_VALUE - traderId);
        final byte[] b2 = Bytes.toBytes(Long.MAX_VALUE - sec);
        final byte[] result = new byte[b1.length + b2.length];
        System.arraycopy(b1, 0, result, 0, b1.length);
        System.arraycopy(b2, 0, result, b1.length, b2.length);
        return result;
    }

    private Date removeTime(final Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date parseDate(String value) throws ParseException {
        final SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy, hh:mm:ss aa");
        return parser.parse(value);
    }
}
