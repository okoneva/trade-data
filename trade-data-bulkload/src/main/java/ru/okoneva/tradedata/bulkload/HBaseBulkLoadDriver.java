package ru.okoneva.tradedata.bulkload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ru.okoneva.tradedata.bulkload.HBaseBulkLoadMapper.TABLE_NAME;

public class HBaseBulkLoadDriver extends Configured implements Tool {

    private static final Logger logger = LoggerFactory.getLogger(HBaseBulkLoadDriver.class);

    /**
     * HBase bulk import
     * Data preparation MapReduce job driver
     * <p>
     * args[0]: HDFS input path
     * args[1]: HDFS output path
     */
    public static void main(String[] args) {
        try {
            final int response = ToolRunner.run(HBaseConfiguration.create(), new HBaseBulkLoadDriver(), args);
            if (response == 0) {
                logger.info("Job is successfully completed");
            } else {
                logger.info("Job failed");
            }
        } catch (Exception e) {
            logger.error("Job Failed", e);
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        int result = 0;
        final String outputPath = args[1];
        final Configuration configuration = getConf();
        final Job job = Job.getInstance(configuration);
        job.setJarByClass(HBaseBulkLoadDriver.class);
        job.setJobName("Bulk Loading HBase Table::" + TABLE_NAME);
        job.setInputFormatClass(TextInputFormat.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapperClass(HBaseBulkLoadMapper.class);
        FileInputFormat.addInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setMapOutputValueClass(Put.class);
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            final TableName tableName = TableName.valueOf(TABLE_NAME);
            final Table table = connection.getTable(tableName);
            HFileOutputFormat2.configureIncrementalLoad(job, table, connection.getRegionLocator(tableName));
            job.waitForCompletion(true);
            if (job.isSuccessful()) {
                HBaseBulkLoad.doBulkLoad(outputPath);
            } else {
                result = -1;
            }
            return result;
        } catch (Exception e) {
            logger.error("Job Failed", e);
            return -1;
        }
    }
}
