package ru.okoneva.tradedata.bulkload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ru.okoneva.tradedata.bulkload.HBaseBulkLoadMapper.TABLE_NAME;

public class HBaseBulkLoad {

    private static final Logger logger = LoggerFactory.getLogger(HBaseBulkLoad.class);

    public static void doBulkLoad(final String pathToHFile) {
        final Configuration configuration = HBaseConfiguration.create();
        configuration.addResource("hbase-site.xml");
        configuration.addResource("mapred-site.xml");
        configuration.addResource("yarn-site.xml");
        configuration.addResource("core-site.xml");
        logger.info("Start Bulk Load");
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            final LoadIncrementalHFiles loadFiles = new LoadIncrementalHFiles(configuration);
            final TableName tableName = TableName.valueOf(TABLE_NAME);
            Table table = connection.getTable(tableName);
            loadFiles.doBulkLoad(
                new Path(pathToHFile), connection.getAdmin(), table, connection.getRegionLocator(tableName)
            );
            logger.info("Bulk Load Completed");
        } catch (Exception e) {
            logger.error("Bulk Load Failed", e);
        }
    }
}
